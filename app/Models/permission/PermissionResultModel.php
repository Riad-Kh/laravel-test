<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PermissionResultModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="PermissionResultModel model",
 *     description="PermissionResultModel model",
 * )
 */
class PermissionResultModel extends Model
{
    protected $fillable = [
         'table' , 'permissions'

    ];

    /**
     * @OA\Property(
     *     description="Table",
     *     title="table",
     * )
     *
     * @var string
     */
    public $table;


    /**
     * @OA\Property(
     *     description="OnePermissionResultModel",
     *     title="permissions",
     * )
     *
     * @var OnePermissionResultModel
     */

    public $OnePermissionResultModel;

}

