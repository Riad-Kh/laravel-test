<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PermissionModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="PermissionModel model",
 *     description="PermissionModel model",
 * )
 */
class PermissionModel extends Model
{
    protected $fillable = [
         'create' , 'edit' , 'browse' , 'show' , 'delete'

    ];

    /**
     * @OA\Property(
     *     description="Create",
     *     title="create",
     * )
     *
     * @var string
     */
    public $create;


    /**
     * @OA\Property(
     *     description="Edit",
     *     title="edit",
     * )
     *
     * @var string
     */
    public $edit;

    /**
     * @OA\Property(
     *     description="Browse",
     *     title="browse",
     * )
     *
     * @var string
     */
    public $browse;

    /**
     * @OA\Property(
     *     description="Show",
     *     title="show",
     * )
     *
     * @var string
     */
    public $show;

    /**
     * @OA\Property(
     *     description="Delete",
     *     title="delete",
     * )
     *
     * @var string
     */
    public $delete;


}

