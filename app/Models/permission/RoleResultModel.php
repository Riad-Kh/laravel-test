<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleResultModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="RoleResultModel model",
 *     description="RoleResultModel model",
 * )
 */
class RoleResultModel extends Model
{
    protected $fillable = [
        'id' , 'name'

    ];

    /**
     * @OA\Property(
     *     description="ID",
     *     title="id",
     * )
     *
     * @var integer
     */
    public $id;


    /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;

}

