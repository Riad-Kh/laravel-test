<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OnePermissionResultModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="OnePermissionResultModel model",
 *     description="OnePermissionResultModel model",
 * )
 */
class OnePermissionResultModel extends Model
{
    protected $fillable = [
        'id' , 'name'

    ];

    /**
     * @OA\Property(
     *     description="ID",
     *     title="id",
     * )
     *
     * @var integer
     */
    public $id;


    /**
     * @OA\Property(
     *     description="Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;
}

