<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SyncRoleModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="SyncRoleModel model",
 *     description="SyncRoleModel model",
 * )
 */
class SyncRoleModel extends Model
{

    /**
     * @OA\Property(
     *     description="Exist Permissions",
     *     title="exist_permissions",
     *     type="array",
     *     @OA\items(
     *          type="integer"
     *     )
     * )
     *
     * @var array
     */

    public $exist_permissions;
    /**
     * @OA\Property(
     *     description="Role id",
     *     title="role_id",
     * )
     *
     * @var integer
     */
    public $role_id;




}

