<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdatePermissionPayload
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="UpdatePermissionPayload model",
 *     description="UpdatePermissionPayload model",
 * )
 */
class UpdatePermissionPayload extends Model
{
    protected $fillable = ['permission_id','table_name' , 'action_type' ];

    /**
     * @OA\Property(
     *     description="id",
     *     title="permission_id",
     * )
     *
     * @var integer
     */
    public $permission_id;

    /**
     * @OA\Property(
     *     description="Table name",
     *     title="table_name",
     * )
     *
     * @var string
     */
    public $table_name;

    /**
     * @OA\Property(
     *     description="Actions",
     *     enum={"create",
     *           "edit",
     *           "show",
     *           "browse",
     *           "delete"},
     *     title="action_type",
     * )
     *
     * @var string
     */
    public $action_type;


}

