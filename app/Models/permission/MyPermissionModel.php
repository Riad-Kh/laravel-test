<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MyPermissionModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="MyPermissionModel model",
 *     description="MyPermissionModel model",
 * )
 */
class MyPermissionModel extends Model
{
    protected $fillable = [
         'table' , 'permissions'
    ];

    /**
     * @OA\Property(
     *     description="Table",
     *     title="table",
     * )
     *
     * @var string
     */
    public $table;


    /**
     * @OA\Property(
     *     description="PermissionModel",
     *     title="permissions",
     * )
     *
     * @var PermissionModel
     */

    public $permissions;

}

