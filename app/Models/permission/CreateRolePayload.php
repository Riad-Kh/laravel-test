<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CreateRolePayload
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CreateRolePayload model",
 *     description="CreateRolePayload model",
 * )
 */
class CreateRolePayload extends Model
{
    /**
     * @OA\Property(
     *     description="Role name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;



}

