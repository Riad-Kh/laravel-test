<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UpdateRoleModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="UpdateRoleModel model",
 *     description="UpdateRoleModel model",
 * )
 */
class UpdateRoleModel extends Model
{

    /**
     * @OA\Property(
     *     description="Role id",
     *     title="id",
     * )
     *
     * @var integer
     */
    public $id;

    /**
     * @OA\Property(
     *     description="Role name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;



}

