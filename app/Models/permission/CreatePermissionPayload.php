<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\permission;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CreatePermissionPayload
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CreatePermissionPayload model",
 *     description="CreatePermissionPayload model",
 * )
 */
class CreatePermissionPayload extends Model
{
    protected $fillable = ['table_name' , 'action_type' ];

    /**
     * @OA\Property(
     *     description="Table name",
     *     title="table_name",
     * )
     *
     * @var string
     */
    public $table_name;

    /**
     * @OA\Property(
     *     description="Actions",
     *     enum={"create",
     *           "edit",
     *           "show",
     *           "browse",
     *           "delete"},
     *     title="action_type",
     * )
     *
     * @var string
     */
    public $action_type;


}

