<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Permission extends Model
{
    use HasFactory;
    protected $guard_name = 'web';

    protected $fillable = [
        'name',
        'guard_name'
    ];
}
