<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['description' , 'deadline','end_flag'];

    public function categories()
    {
        return $this->belongsToMany(Category::class,'task_categories');
    }

    public function sub_task()
	{
		return $this->hasMany(SubTask::class);
	}
}
