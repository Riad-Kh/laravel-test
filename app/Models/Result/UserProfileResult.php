<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\Result;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProfileResult
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="ProfileResult model",
 *     description="ProfileResult model",
 * )
 */
class UserProfileResult extends Model
{
    protected $fillable = [
        'id' , 'name' , 'email'

    ];

    /**
     * @OA\Property(
     *     description="ID",
     *     title="id",
     * )
     *
     * @var integer
     */
    public $id;


    /**
     * @OA\Property(
     *     description=" Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;


     /**
     * @OA\Property(
     *     description="email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;


}

