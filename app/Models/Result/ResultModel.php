<?php

/**
 * @license Apache 2.0
 */


namespace App\Models\Result;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Result
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="Result model",
 *     description="Result model",
 * )
 */

class ResultModel extends Model
{
    protected $fillable = ['isOk' , 'message' , 'result' ];
     /**
     * @OA\Property(
     *      type="array",
     *  @OA\Items(
     *          type="array",
     *          @OA\Items()
     *      ),
     *     description="ResultModel Model",
     *     title="result",
     * )
     *
     * @var array
     */
    public $result;

    /**
     * @OA\Property(
     *     description="Indicates if the response is ok or not",
     *     title="isOk",
     * )
     *
     * @var boolean
     */
    public $isOk;

    /**
     * @OA\Property(
     *     description="Api message",
     *     title="message",
     * )
     *
     * @var string
     */
    public $message;


}
