<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\Result;



use App\Models\API\lists\MediaModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminProfileResult
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="AdminProfileResult model",
 *     description="AdminProfileResult model",
 * )
 */
class AdminProfileResult extends Model
{
    protected $fillable = [
        'id' , 'firstname' , 'lastname' , 'phone' , 'email' , 'image' , 'language' , 'role'

    ];

    /**
     * @OA\Property(
     *     description="ID",
     *     title="id",
     * )
     *
     * @var integer
     */
    public $id;


    /**
     * @OA\Property(
     *     description="First Name",
     *     title="firstname",
     * )
     *
     * @var string
     */
    public $firstname;


    /**
     * @OA\Property(
     *     description="Last Name",
     *     title="lastname",
     * )
     *
     * @var string
     */
    public $lastname;

    /**
     * @OA\Property(
     *     description="Phone",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;

     /**
     * @OA\Property(
     *     description="email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     description="iamge",
     *     title="image",
     * )
     *
     * @var string
     */
    public $image;

    /**
     * @OA\Property(
     *     description="language",
     *     title="language",
     * )
     *
     * @var string
     */
    public $language;

    /**
     * @OA\Property(
     *     description="role",
     *     title="role",
     * )
     *
     * @var string
     */
    public $role;










}

