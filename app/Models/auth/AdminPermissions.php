<?php

namespace App\Models\auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class AdminPermissions
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="AdminPermissions model",
 *     description="AdminPermissions model",
 * )
 */
class AdminPermissions extends Model
{

    protected $fillable = [
        'role' , 'permissions'
   ];
    /**
     * @OA\Property(
     *     description="Role",
     *     title="role",
     * )
     *
     * @var RoleResultModel
     */

    public $role;


    /**
     * @OA\Property(
     *     description="My Permission",
     *     title="my_permission",
     * )
     *
     * @var MyPermissionModel
     */

    public $my_permission;


}
