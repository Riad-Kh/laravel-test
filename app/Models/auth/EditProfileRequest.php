<?php

namespace App\Models\auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class EditProfileRequest
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="EditProfileRequest model",
 *     description="EditProfileRequest model",
 * )
 */
class EditProfileRequest extends Model
{
   /**
     * @OA\Property(
     *     description="First Name",
     *     title="first_name",
     * )
     *
     * @var string
     */
    public $first_name;

    /**
     * @OA\Property(
     *     description="Last Name",
     *     title="last_name",
     * )
     *
     * @var string
     */
    public $last_name;

    /**
     * @OA\Property(
     *  description="User image",
     *  property="image",
     *  type="string",
     *  format="binary"
     * )
     *
     */

    public $image;

    /**
     * @OA\Property(
     *     description="phone",
     *     title="phone",
     * )
     *
     * @var string
     */
    public $phone;

    /**
     * @OA\Property(
     *     description="email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;
}
