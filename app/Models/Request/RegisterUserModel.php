<?php

/**
 * @license Apache 2.0
 */

namespace App\Models\Request;
use Illuminate\Database\Eloquent\Model;


/**
 * Class RegisterUserModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="RegisterUserModel model",
 *     description="RegisterUserModel model",
 * )
 */

class RegisterUserModel extends Model
{



    /**
     * @OA\Property(
     *     description=" Name",
     *     title="name",
     * )
     *
     * @var string
     */
    public $name;


    /**
     * @OA\Property(
     *     description="Email",
     *     title="email",
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     description="Password",
     *     title="password",
     * )
     *
     * @var string
     */
    public $password;

}

