<?php

namespace App\Models\task;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * Class CreateTaskModel
 *
 * @package Petstore30
 *
 * @OA\Schema(
 *     title="CreateTaskModel model",
 *     description="CreateTaskModel model",
 * )
 */
class CreateTaskModel extends Model
{

    /**
     * @OA\Property(
     *     description="Description",
     *     title="description",
     * )
     *
     * @var string
     */
    public $description;

    /**
     * @OA\Property(
     *     description="Dead Line",
     *     title="deadline",
     * )
     *
     * @var date
     */
    public $deadline;


    /**
     * @OA\Property(
     *     description="End - NotEnd Property,  0=> NotEnd, 1 => End",
     *     enum={"0", "1"},
     *     title="status",
     * )
     *
     * @var integer
     */
    public $end_flag;


    /**
     * @OA\Property(
     *     description="Categories",
     *     title="categories",
     *     type="array",
     *     @OA\items(
     *          type="integer"
     *     )
     * )
     *
     * @var array
     */

     public $categories;


    /**
     * @OA\Property(
     *     description="Sub Tasks",
     *     title="sub_tasks",
     *     type="array",
     *     @OA\items(
     *          type="string"
     *     )
     * )
     *
     * @var array
     */

     public $sub_tasks;


}
