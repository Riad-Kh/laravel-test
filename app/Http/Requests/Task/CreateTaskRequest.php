<?php

namespace App\Http\Requests\Task;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'description' => 'required|string',
            'deadline' => 'required|date_format:Y-m-d',
            'end_flag' => 'required|boolean',
            'categories' => 'required|array',
            'categories.*' => [
                'required',
                'exists:categories,id',
            ],
            'sub_tasks' => 'array|nullable',
            'sub_tasks.*' => 'string',
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(returnError($validator->errors()->all()));
    }
}
