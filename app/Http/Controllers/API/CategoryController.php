<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\services\CategoryServices;


/**
 * Class CategoryController
 */
class CategoryController extends Controller
{



     /**
      * @OA\Get(path="/category/get-all",
      *     tags={"Category"},
      *     summary="Get categries",
      *     security={{"apiAuth":{}}},
      *     @OA\Parameter(
      *         name="Language",
      *         in="header",
      *         description="(en or ar) If left empty it is English",
      *         required=false,
      *         @OA\Schema(
      *             type="string"
      *         )
      *     ),
      *     @OA\Response(
      *         response = 200,
      *         description = "ApiResult response",
      *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
      *     ),
      * )
      */

     public function index()
     {
         return CategoryServices::index();
     }


}
