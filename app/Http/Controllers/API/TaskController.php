<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Task\CreateTaskRequest;
use App\Http\Requests\Task\GetTaskRequest;
use App\services\AuthServices;
use App\services\TaskServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacadesRequest;

/**
 * Class TaskController
 */
class TaskController extends Controller
{
/**
     * @OA\Post(path="/task/create",
     *     tags={"Task"},
     *     summary="Create a task",
     *     security={{"apiAuth":{}}},
     *     operationId="Createtask",
     *     @OA\RequestBody(
     *         description="CreateRole model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/CreateTaskModel")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param CreatetaskRequest $request
     * @return string
     */

     public function create(CreateTaskRequest $request)
     {
         return TaskServices::create($request);
     }


     /**
      * @OA\Get(path="/task/get-all",
      *     tags={"Task"},
      *     summary="Get tasks",
      *     security={{"apiAuth":{}}},
      *     @OA\Parameter(
      *         name="search",
      *         in="header",
      *         description="Search in task",
      *         required=false,
      *         @OA\Schema(
      *             type="string"
      *         )
      *     ),
      *     @OA\Response(
      *         response = 200,
      *         description = "ApiResult response",
      *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
      *     ),
      * )
      */

     public function index(Request $request)
     {
         return TaskServices::index($request);
     }



     /**
      * @OA\Get(path="/task/get-one",
      *     tags={"Task"},
      *     summary="Get task",
      *     security={{"apiAuth":{}}},
      *     @OA\Parameter(
      *         name="id",
      *         in="query",
      *         description="taskId To Show It",
      *         required=true,
      *         @OA\Schema(
      *             type="string"
      *         )
      *     ),
      *     @OA\Response(
      *         response = 200,
      *         description = "ApiResult response",
      *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
      *     ),
      * )
      */

     public function show(GetTaskRequest $request)
     {
         return TaskServices::show($request);
     }


}
