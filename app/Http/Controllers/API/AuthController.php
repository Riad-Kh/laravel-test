<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\services\AuthServices;
use Illuminate\Http\Request;

/**
 * Class AuthController
 */
class AuthController extends Controller
{

    /**
     * @OA\Post(path="/user-register",
     *     tags={"Auth"},
     *     summary="Register as a new user",
     *     operationId="authRegister",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\RequestBody(
     *         description="Registration model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/RegisterUserModel")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "RegisterResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param RegisterRequest $request
     * @return string
     */

    public function user_register(RegisterRequest $request)
    {
      return AuthServices::User_Register($request);

    }

    /**
     * @OA\Post(path="/user-login",
     *     tags={"Auth"},
     *     summary="Login as a user",
     *     operationId="authLogin",
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),

     *     @OA\RequestBody(
     *         description="Login model",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginModel")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "User Profile response",
     *         @OA\JsonContent(ref="#/components/schemas/ResultModel"),
     *     ),
     * )
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function user_login(LoginRequest $request)
    {
       return AuthServices::User_Login($request);
    }

    /**
     * @OA\Post(path="/logout",
     *     tags={"Auth"},
     *     summary="Log out from system",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @param $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        return AuthServices::User_logut($request);
    }



    /**
     * @OA\Get(path="/profile",
     *     tags={"Auth"},
     *     summary="Get profile details",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "ApiResult response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function profile()
    {
        return AuthServices::User_Profile();
    }

    /**
     * @OA\Delete(path="/delete-account",
     *     tags={"Auth"},
     *     summary="Delete as a user",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     * @return JsonResponse
     */

    public function delete_account(Request $request)
    {
        return AuthServices::DeleteAccount($request);
    }



     /**
     * @OA\Get(path="/check-token",
     *     tags={"Auth"},
     *     summary="Test a token for expiration",
     *     security={{"apiAuth":{}}},
     *     @OA\Parameter(
     *         name="Language",
     *         in="header",
     *         description="(en or ar) If left empty it is English",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Success response",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResult"),
     *     ),
     * )
     */

    public function check_token()
    {
        return AuthServices::checkToken();

    }


}
