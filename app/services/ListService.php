<?php

namespace App\services;


use App\enums\ErrorCode;
use App\Models\API\auth\LoginResultApi;
use App\Models\API\lists\CategoryListResult;
use App\Models\API\lists\CategoryResult;
use App\Models\API\lists\CityResult;
use App\Models\API\lists\HotelResult;
use App\Models\API\lists\ReservationResult;
use App\Models\API\lists\PostResult;
use App\Models\API\lists\SettingResult;
use App\Models\API\other\ApiMessage;
use App\Models\AppSetting;
use App\Models\Banner;
use App\Models\Category;
use App\Models\City;
use App\Models\Hotel;
use App\Models\Post;
use App\Models\PostCategories;
use App\Models\PostCategory;

class ListService
{
    public static function hotels($request)
    {
        try {

            $hotels = Hotel::query();

            self::filterHotel($hotels , $request);

            $data = [];
            foreach ($hotels->get() as $one) {
                $item = FillApiModelService::FillHotelsApiModel($one);
                $data[] = $item;
            }

            $result = new HotelResult([
                'result' => $data,
                'isOk' => true,
                'message' => new ApiMessage([
                    'type' => 'Success',
                    'code' => ErrorCode::success,
                    'content' => '',
                ])
            ]);

            return [true, $result, '', ''];

        } catch (\Exception $ex) {
            return [false, null, UserRepository::Msg_Exception, $ex->getMessage()];
        }

    }

    public static function categories($request)
    {
        try {

            $categories = Category::query();

            self::filterCategory($categories , $request);

            $data = [];
            foreach ($categories->get() as $one) {
                $item = FillApiModelService::FillCategoriesListApiModel($one);
                $data[] = $item;
            }

            $result = new CategoryListResult([
                'result' => $data,
                'isOk' => true,
                'message' => new ApiMessage([
                    'type' => 'Success',
                    'code' => ErrorCode::success,
                    'content' => '',
                ])
            ]);

            return [true, $result, '', ''];

        } catch (\Exception $ex) {
            return [false, null, UserRepository::Msg_Exception, $ex->getMessage()];
        }

    }

    public static function cities($request)
    {
        try {

            $cities = City::query();

            self::filterCategory($cities , $request);

            $data = [];
            foreach ($cities->get() as $one) {
                $item = FillApiModelService::FillCityApiModel($one);
                $data[] = $item;
            }

            $result = new CityResult([
                'result' => $data,
                'isOk' => true,
                'message' => new ApiMessage([
                    'type' => 'Success',
                    'code' => ErrorCode::success,
                    'content' => '',
                ])
            ]);

            return [true, $result, '', ''];

        } catch (\Exception $ex) {
            return [false, null, UserRepository::Msg_Exception, $ex->getMessage()];
        }

    }

    public static function posts($request)
    {
        try {

            $posts = Post::query();

            self::filterPost($posts , $request);

            $data = [];
            foreach ($posts->get() as $one) {
                $item = FillApiModelService::FillPostApiModel($one);
                $data[] = $item;
            }

            $result = new PostResult([
                'result' => $data,
                'isOk' => true,
                'message' => new ApiMessage([
                    'type' => 'Success',
                    'code' => ErrorCode::success,
                    'content' => '',
                ])
            ]);

            return [true, $result, '', ''];

        } catch (\Exception $ex) {
            return [false, null, UserRepository::Msg_Exception, $ex->getMessage()];
        }

    }

    public static function settings($request)
    {
        try {

            $banners = Banner::query()->get();
//            return [true, $banners->first()->building, '', ''];
            $data = FillApiModelService::FillSettingApiModel($banners);

            $result = new SettingResult([
                'result' => $data,
                'isOk' => true,
                'message' => new ApiMessage([
                    'type' => 'Success',
                    'code' => ErrorCode::success,
                    'content' => '',
                ])
            ]);

            return [true, $result, '', ''];

        } catch (\Exception $ex) {
            return [false, null, UserRepository::Msg_Exception, $ex->getMessage()];
        }

    }

    public static function filterHotel($hotels , $request){
        if (isset($request->id)) {
            $hotels->where(['id' => $request->id]);
        }

        if (isset($request->title)) {
            $hotels->where('title', 'like', '%' . $request->title . '%');
        }

        if (isset($request->sub_category_id)) {
            $hotels->where(['subcategory_id' => $request->sub_category_id]);
        }

        if (isset($request->city_id)) {
            $hotels->where(['city_id' => $request->city_id]);
        }

        if (isset($request->category_id)) {
            $hotels->whereHas('subcategory' , function ($query) use ($request){
                $query->whereHas('category' , function ($query2) use ($request){
                    $query2->where(['id' => $request->category_id]);
                });
            });
        }

        if (isset($request->category_ids)) {
            $hotels->whereHas('subcategory' , function ($query) use ($request){
                $query->whereHas('category' , function ($query2) use ($request){
                    $query2->whereIn('id' , $request->category_ids);
                });
            });
        }

        if (isset($request->mix)) {
            $hotels->inRandomOrder();
        }
    }

    public static function filterCategory($category , $request){

        if (isset($request->id)) {
            $category->where(['id' => $request->id]);
        }

    }

    public static function filterPost($post , $request){

        if (isset($request->id)) {
            $post->where(['id' => $request->id]);
        }

        if (isset($request->title)) {
            $post->where('title', 'like', '%' . $request->title . '%');
        }

        if (isset($request->hotel_id)) {
            $post->where(['hotel_id' => $request->hotel_id]);
        }

        if (isset($request->type)) {
            $post->where(['type' => $request->type]);
        }

        if (isset($request->category_id)) {
            $post->whereHas('hotel' , function ($query) use ($request){
                $query->whereHas('subcategory' , function ($query2) use ($request){
                    $query2->where('category_id' , $request->category_id);
                });
            });
        }

        if (isset($request->city_id)) {
            $post->whereHas('hotel' , function ($query) use ($request){
                $query->where('city_id' , $request->city_id);
            });
        }

    }

}
