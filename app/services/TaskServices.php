<?php

namespace App\services;

use App\enums\LoginApiEnum;
use App\Models\Result\ResultModel;
use Illuminate\Support\Facades\Auth;
use App\Models\Result\LoginResultModel;
use App\Models\Task;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class TaskServices
{

    const Error_Message = "Some Thing is Wrong";
    const Successfully = "Successfully";

    public static function create($request)
    {
        try {

            $request = array_filter($request->validated());

            DB::beginTransaction();

                $task = Task::query()->create($request);

                $task->categories()->attach($request['categories']);

                if (!empty($request['sub_tasks'])) {
                    $subTasksData = collect($request['sub_tasks'])->map(function ($subTaskDescription) {
                        return ['description' => $subTaskDescription];
                    })->toArray();

                    foreach ($subTasksData as &$subTask) {
                        $subTask['task_id'] = $task->id;
                    }


                    $task->sub_task()->createMany($subTasksData);
                }

            DB::commit();

                 return returnData(ResultModel::class , $task ,self::Successfully);

        }catch (\Exception $ex) {

            DB::rollback();
            return returnError(self::Error_Message , $ex->getMessage() , $ex->getCode());
        }

    }


    public static function index($request)
    {
        try{

            $tasks = Task::with(['categories', 'sub_task'])
            ->when($request->filled('search'), function ($query) use ($request) {
                $searchTerm = $request->input('search');
                $query->where('description', 'like', "%$searchTerm%")
                    ->orWhereHas('sub_task', function ($subQuery) use ($searchTerm) {
                        $subQuery->where('description', 'like', "%$searchTerm%");
                    })
                    ->orWhereHas('categories', function ($subQuery) use ($searchTerm) {
                        $subQuery->where('name', 'like', "%$searchTerm%");
                    });
            })
            ->get();

            return returnData(ResultModel::class , $tasks ,self::Successfully);

        }catch (\Exception $ex) {
            return returnError(self::Error_Message , $ex->getMessage() , $ex->getCode());
        }
    }

    public static function show($request)
    {
        try{

            $tasks = Task::with(['categories', 'sub_task'])->findOrFail($request->id);

            return returnData(ResultModel::class , $tasks ,self::Successfully);

        }catch (\Exception $ex) {
            return returnError(self::Error_Message , $ex->getMessage() , $ex->getCode());
        }
    }
}
