<?php

namespace App\services;

use App\enums\LoginApiEnum;
use App\enums\ProductLikeEnum;
use App\enums\UserCofirmCodeType;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Product;
use App\Models\Result\ListResultModel;
use App\Models\Result\ResultModel;
use Illuminate\Support\Facades\Auth;
use App\services\FillApiModels;
use App\Models\Result\LoginResultModel;
use App\Models\User;
use App\Models\UserFavorateProduct;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth as FacadesJWTAuth;
// use Tymon\JWTAuth\Facades\JWTAuth;


class AuthServices
{
    const Error_Message = "Some Thing is Wrong";
    const Login_Successfully_Message= " Loged in Successfully";
    const Registration_Success_Message = "Msg_RegistrationSuccess";
    const Successfully = "Successfully";

    public static function User_Login ($request)
    {
        try{
            $login_model = new LoginResultModel();
            if (Auth::attempt(['email' => request('email'), 'password' => request('password')])){

                     $user = User::find(Auth::user()->id);


                     $token = $user->createToken('appToken')->accessToken;

            }else{
                return returnError(LoginApiEnum::LabelOf(LoginApiEnum::invalid_credentials));
            }
               $login_model['profile'] = FillApiModelService::FillUserProfileModel($user);
               $login_model['token'] = $token;

               return returnData(ResultModel::class , $login_model ,self::Login_Successfully_Message);


          }catch (\Exception $ex) {
            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
          }
    }

    public static function User_Register($request)
    {
        try {

            $user = User::query()->create($request->validated());
        //    return self::User_Login($request);
            return returnSuccess(AuthServices::Registration_Success_Message);

        } catch (\Exception $ex) {

            return returnError(self::Error_Message, $ex->getMessage(), $ex->getCode());
        }

    }

    public static function User_logut($request)
    {

        try {
            $request->user()->token()->revoke();
            return returnSuccess("Logged out successfully");
        } catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


    public static function Update_Profile($request)
    {
        try {

           $filterRequest = array_filter($request->validated());
            $model = user();
            if ($model) {

               if(isset($request['image']) && $model->image != null){
                    deleteImage($model->image);
                     $filterRequest['image'] = uploadImage($request->image, User::image_directory);
                }

                $model->update($filterRequest);

                return self::User_Login($request);

                } else {
                return returnError("User not found");
            }

        } catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), 555);
        }
    }


    public static function DeleteAccount($request)
    {
        try {
            $model = user();
            if ($model) {
                $request->user()->token()->revoke();
                $model->delete();
            } else {
                return returnError("User not found");
            }
            return returnSuccess("Deleted");

        } catch (\Exception $ex) {
            return returnError(AuthServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }

    public static function checkToken()
    {
        if (!\user()) {
            return returnError(trans('Token expired'));
        }

        return returnSuccess(trans('Token Valid'));
    }












}
