<?php

namespace App\services;

use App\Models\auth\AdminPermissions;
use App\Models\permission\OnePermissionResultModel;
use App\Models\permission\PermissionResultModel;
use App\Models\permission\RoleResultModel;
use App\Models\Result\AdminProfileResult;
use App\Models\Result\UserProfileResult;

class FillApiModelService
{

    public static function FillUserProfileModel($item)
    {
        return new UserProfileResult([
            'id' => $item->id,
            'name' => $item->name,
            'email' => $item->email,
        ]);
    }

    public static function FillAdminProfileModel($item)
    {
        return new AdminProfileResult([
            'id' => $item->id,
            'firstname' => $item->first_name,
            'lastname' => $item->last_name,
            'phone' => $item->phone,
            'email' => $item->email,
            'image' => $item->image ? getImage($item->image ) : null,
            'language' => $item->language,
            'role' => $item->role->name
        ]);
    }


    public static function FillRolesModel($items)
    {
        $roles = [];
        foreach ($items as $one){
            $roles[] = self::FillOneRole($one);
        }
        return $roles;
    }

    public static function FillOneRole($item)
    {
        return new RoleResultModel([
            'id' => $item->id,
            'name' => $item->name,
        ]);
    }

    public static function FillPermissionsModel($items,$title)
    {
            $permission =new PermissionResultModel([
                'table' => $title,
                'permissions' =>  self::FillPermissions($items),
            ]);

        return $permission;
    }

    public static function FillPermissions($item)
    {
        $permission = [];
            foreach ($item as $one){
                $permission [] =  new OnePermissionResultModel([
                    'id' => $one->id,
                    'name' => $one->name,
                ]);
            }
        return $permission;
    }


    public static function FillMyPermissionsModel($items,$title)
    {
            return new PermissionResultModel([
                'table' => $title,
                'permissions' =>  FillPermission($title ,$items),
            ]);


    }

}
