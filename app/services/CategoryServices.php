<?php

namespace App\services;

use App\Models\Category;
use App\Models\Result\ResultModel;




class CategoryServices
{
    const Error_Message = "Some Thing is Wrong";

    const Successfully = "Successfully";

    public static function index()
    {
        try {

            $data = Category::query()->latest()->get();

               return returnData(ResultModel::class , $data ,self::Successfully);


        }catch (\Exception $ex) {
            return returnError(CategoryServices::Error_Message, $ex->getMessage(), $ex->getCode());
        }
    }


}
