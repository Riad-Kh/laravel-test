<?php


use App\enums\ErrorCode;
use App\enums\PermissionTypesNames;
use App\enums\YesNoEnum;
use App\Models\Result\ApiMessage;
use App\Models\Result\ApiResult;
use App\Models\API\other\IdValueApiModel;
use App\Models\permission\PermissionModel;
use App\services\FillApiModelService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\URL;
use Modules\Auth\Entities\GlobalUser;
use Modules\Auth\Entities\User;

function getCurrentLang()
{
    return app()->getLocale();
}

function returnData($model , $data , $message){
    return response()->json(new $model([
        'items_count' => is_array($data) ? count($data) : null,
        'isOk' => true,
        'result' => $data,
        'message' => $message
    ]));
}

function returnError($msg, $ex = '' , $errNum = \App\enums\ErrorCode::error , $statusCode = 400)
{
    if($ex != '')
        $statusCode = 500;

    return response()->json(api_error_msg($msg , $errNum , 'Error' , $ex) , $statusCode);
}

function returnSuccess($msg , $ex = '' , $errNum = \App\enums\ErrorCode::success )
{
    return response()->json(api_error_msg($msg , $errNum , 'Success' , $ex));
}

function stopv($obj = '', $msg = '') {
    echo $msg ? "$msg<br/>" : "";
    var_dump($obj);
    die();
}

function api_error_msg($messages, $code , $type , $ex) {
    if(!$code){
        $code = \App\enums\ErrorCode::error;
    }

    if(is_array($messages)) {
        $err=[];
        foreach ($messages as $one) {
            $one = is_array($one) ? implode($one) : $one;

            if (user()) {
                if(user()->language == 'en')
                    $one = Lang::get($one, [], user()->language);
                else
                    $one = Lang::get('validation.' . $one, [], user()->language);
            } else {
                $one = Lang::get('validation.' . $one, [], 'en');
            }

            $err[] = $one;
        }
        $messages = $err;
    } else {
        if (user()) {
            $messages = Lang::get('all.' . $messages, [], user()->language);
        } else {
            $messages = Lang::get('all.' . $messages, [], 'en');
        }
        $messages = array($messages);
    }

    return new ApiResult([
        'isOk' => $type == 'Success',
        'message' => $messages,
        'exception' => $ex
    ]);
}


function user() {
    return Auth::guard('user-api')->user();
}

function full_user($withUser = [] , $withGlobalUser = [] , $user= null) {
    if($user == null)
        $user = user();

    $global_user = GlobalUser::with($withGlobalUser)->find($user->id)->toArray();
    $global_user['progress'] = $user->progress();
    $global_user['type'] = $user->type();
    $global_user['url'] = $user->url;
    $user = $global_user['owner_type']::with($withUser)->find($user->owner->id)->toArray();
    return array_merge($user , $global_user);
}

function admin() {
    return Auth::guard('admin-api')->user();
}

function sys_admin() {
    return Auth::guard('web')->user();
}

function uploadImage($image , $fileName)
{
    $rand = rand(0 , 9999999);
    $imageName =  time() . $rand .'.'. $image->extension();
    $imageToSave = $fileName . '/' . time(). $rand . '.'. $image->extension();
    $image->move(public_path('storage'. DIRECTORY_SEPARATOR .$fileName), $imageName);
    return $imageToSave;
}

function getImage($image)
{
    if(isset($image) && !empty($image)) {
        $image = str_replace('\\\\', '/', $image);
        $image = str_replace('\\', '/', $image);
        return URL::to('/') . '/storage/' . $image ;
    }
    else return null;
}


function mapper($class , $item){

    $obj = new $class();

    foreach ($obj->getFillable() as $attr){
        $obj[$attr] = $item->$attr;
    }


    foreach ($obj->getRelations() as $attr => $className){
        if(isset($item->$attr)) {
            $rel = $item->$attr;
            $data = [];

            try {
                try {
                    foreach ($rel as $one)
                        $data[] = mapper($className, (object)$one);
                } catch (Exception $ex) {
                    $data = mapper($className, (object)$rel);
                }
            }catch (Exception $ex){
                $data = [];
            }

            $obj[$attr] = $data;
        }
    }

    if(method_exists($class , 'getMedia'))
        foreach ($obj->getMedia() as $attr){
            if(strpos($item->$attr, '[') !== false){
                $images = str_replace('"' , '' , $item->$attr);
                $images = str_replace('[' , '' , $images);
                $images = str_replace(']' , '' , $images);
                $images = str_replace('\\\\' , '/' , $images);
                $images = str_replace('\\' , '/' , $images);
                $images = explode(',' , $images);
                $data = [];
                foreach ($images as $image)
                    $data[] = getImage($image);
                $obj[$attr] = $data;
            }
            else
                $obj[$attr] = getImage($item->$attr);
        }

    if(method_exists($class , 'getEnums'))
        foreach ($obj->getEnums() as $attr => $enumClass){
            $obj[$attr] = checkFillIdValueApiModel($item->$attr , $enumClass);
        }

    return $obj;
}


if (!function_exists('menu_display')) {
    function menu_display($menuName, $type = null, array $options = [])
    {
        return \App\Models\Menu::display($menuName, $type, $options);
    }
}


function mapperOwnerGuard(){
    return [
        \Modules\Auth\Entities\User::class => 'user-api' ,
        \Modules\Auth\Entities\ItCompany::class => 'it-company-api',
        \Modules\Auth\Entities\ClientCompany::class => 'client-company-api',
    ];
}

function mapperOwnerType(){
    return [
        \Modules\Auth\Entities\User::class => 0 ,
        \Modules\Auth\Entities\ItCompany::class => 1,
        \Modules\Auth\Entities\ClientCompany::class => 2,
    ];
}

function checkFillIdValueApiModel($attr , $enumModel = YesNoEnum::class){
    return isset($attr)
        ? FillIdValueApiModel($attr, $enumModel::LabelOf($attr))
        : $attr;
}

function FillIdValueApiModel($id, $value)
{
    return new IdValueApiModel([
        'id' => $id,
        'value' => $value ? $value : '',
    ]);
}

function deleteImage($image){
    $image_path = public_path().'/storage/'.$image;
    if(file_exists($image_path))
        unlink($image_path);
}

function FillPermission($title, $permissions)
{
    $result = new PermissionModel([
        'create' => false ,
        'edit'=> false ,
        'browse' => false,
        'show' => false,
        'delete'=> false
    ]);


    foreach ($permissions as $permission)
    {
        if($permission->name == PermissionTypesNames::create.'.'.$title)
        $result['create'] = true;

        if($permission->name == PermissionTypesNames::edit.'.'.$title)
        $result['edit'] = true;

        if($permission->name == PermissionTypesNames::delete.'.'.$title)
        $result['delete'] = true;

        if($permission->name == PermissionTypesNames::browse.'.'.$title)
        $result['browse'] = true;

        if($permission->name == PermissionTypesNames::show.'.'.$title)
        $result['show'] = true;

    }
    return $result;
}


