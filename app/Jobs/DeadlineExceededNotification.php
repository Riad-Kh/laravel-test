<?php

namespace App\Jobs;

use App\Models\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class DeadlineExceededNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $taskId;

    /**
     * Create a new job instance.
     *
     * @param int $taskId
     */
    public function __construct($taskId)
    {
        $this->taskId = $taskId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $task = Task::find($this->taskId);


        if ($task && $task->deadline < now() && !$task->end_flag) {

            Mail::to('user email ')->send(new \App\Mail\DeadlineExceeded($task));


            $task->update(['end_flag' => true]);
        }
    }


}
