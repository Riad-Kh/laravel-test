<?php

namespace App\enums;

class PermissionTypesNames extends PhpEnum {

    const create = 'create';
    const edit = 'edit';
    const delete = 'delete';
    const browse = 'browse';
    const show = 'show';

}
