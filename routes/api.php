<?php

use App\Http\Controllers\Admin\AdminAuthController;
use App\Http\Controllers\Admin\PermissionRoleController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\TaskController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Models\Permission as ModelsPermission;
use Spatie\Permission\Models\Role;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user-login', [AuthController::class,'user_login']);
Route::post('user-register', [AuthController::class,'user_register']);

Route::group(['middleware' => ['auth:user-api']] , function (){

    Route::get('profile', [AuthController::class,'profile']);
    Route::post('logout' , [AuthController::class,'logout']);
    Route::delete('delete-account', [AuthController::class,'delete_account']);
    Route::get('check-token',[AuthController::class,'check_token']);
});


Route::group(['prefix' => 'task'] , function(){
    Route::group(['middleware' => ['auth:user-api']] , function (){
        Route::post('create', [TaskController ::class,'create']);
        Route::get('get-all',[TaskController::class,'index']);
        Route::get('get-one',[TaskController::class,'show']);
        });
});

Route::group(['prefix' => 'category'] , function(){
    Route::group(['middleware' => ['auth:user-api']] , function (){
        Route::get('get-all',[CategoryController::class,'index']);
        });
});







