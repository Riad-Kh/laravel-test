<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task Deadline Exceeded</title>
</head>
<body>
    <div>
        <h2>Task Deadline Exceeded</h2>
        <p>
            The deadline for the task has been exceeded. Please take necessary actions.
        </p>
        <p>
            Task Details:
        </p>
        <ul>
            <li><strong>Task ID:</strong> {{ $task->id }}</li>
            <li><strong>Description:</strong> {{ $task->description }}</li>
            <li><strong>Deadline:</strong> {{ $task->deadline }}</li>

        </ul>
        <p>
            You can view the task details by logging into your application.
        </p>
        <p>
            Thank you!
        </p>
    </div>
</body>
</html>
